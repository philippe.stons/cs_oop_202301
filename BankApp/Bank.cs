﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public class Bank
    {
        public string Name { get; set; }

        Dictionary<string, BaseAccount> _BaseAccounts = new Dictionary<string, BaseAccount>();

        public KeyValuePair<string, BaseAccount>[] BaseAccounts
        {
            get { return _BaseAccounts.ToArray(); }
        }

        public BaseAccount this[string Numero]
        {
            get
            {
                BaseAccount c;
                _BaseAccounts.TryGetValue(Numero, out c);
                return c;
            }
        }

        public void Add(BaseAccount c)
        {
            _BaseAccounts.Add(c.Number, c);
        }

        public void Delete(string Numero)
        {
            _BaseAccounts.Remove(Numero);
        }

        public double AccountSavings(Person person)
        {
            double savings = 0.0;

            foreach(BaseAccount acc in _BaseAccounts.Values)
            {
                if(acc.Owner == person) 
                {
                    savings = savings + acc;
                }
            }

            return savings;
        }
    }
}

