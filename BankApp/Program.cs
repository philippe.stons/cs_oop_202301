﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public class Car
    {
        public void replaceTyre() { 
            // logic
        }

        public static void StaticReplaceType(Car _this) 
        {
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Car c = new Car();

            c.replaceTyre(); // c.replaceTyre(c => this)

            Car.StaticReplaceType(c);

            BaseAccount acc = new Account("123456789", new Person("Hello", "World", DateTime.Now));

            try
            {
               acc.Withdraw(100);
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // GenericTest<int> test = new GenericTest<int>(55);
            GenericTest<Account> test2 = new GenericTest<Account>((Account) acc);
            test2.MyMethod<long>();

            Console.ReadLine();
        }
    }
}
