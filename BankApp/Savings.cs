﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public class Savings: BaseAccount
    {
        public DateTime LastWithdrawal
        {
            get;
            private set;
        }

        public Savings(string Number, Person Owner) : this(Number, Owner, DateTime.Now, 0D)
        {

        }

        public Savings(string Number, Person Owner, DateTime LastWithdrawal, double Balance) : base(Number, Owner, Balance)
        {
            this.LastWithdrawal = LastWithdrawal;
        }

        public override void Withdraw(double Amount)
        {
            double oldAmount = Amount;
            base.Withdraw(Amount);

            if (this.Balance != oldAmount)
            {
                LastWithdrawal = DateTime.Now;
            }
        }

        protected override double ComputeInterest()
        {
            return Balance * 0.045;
        }
    }
}
