﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public class Person
    {
        public string LastName { get; private set; }
        public string FirstName { get; private set; }
        public DateTime BirthDate { get; private set; }

        public Person(string LastName, string FirstName, DateTime BirthDate)
        {
            this.LastName  = LastName;
            this.FirstName = FirstName;
            this.BirthDate = BirthDate;
        }
    }
}
