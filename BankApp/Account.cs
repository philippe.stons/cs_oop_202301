﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public class Account: BaseAccount
    {
        private double _CreditLine;

        public double CreditLine
        {
            get { return _CreditLine; }
            private set
            {
                if (value < 0)
                    return; // to replace with an exception

                _CreditLine = value;
            }
        }

        public Account(string Number, Person Owner) : this(Number, Owner, 0D)
        {

        }

        public Account(string Number, Person Owner, double Balance) : base(Number, Owner, Balance)
        {

        }

        public override void Withdraw(double Amount)
        {
            base.Withdraw(Amount, CreditLine);
        }

        protected override double ComputeInterest()
        {
            return Balance * (Balance > 0 ? 0.03 : 0.0975);
        }
    }
}
