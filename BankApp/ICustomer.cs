﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    internal interface ICustomer
    {
        double Balance
        {
            get;
        }

        void Withdraw(double Amount);
        void Deposit(double Amount);
    }
}
