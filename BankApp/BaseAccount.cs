﻿using BankApp.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public abstract class BaseAccount : IBanker, ICustomer
    {
        // Account + Account + Account
        // Account + Account => return a double
        // so this becomes :
        // double + Account
        public static double operator +(BaseAccount acc1, BaseAccount acc2)
        {
            return ((acc1.Balance < 0.0) ? 0.0 : acc1.Balance) + ((acc2.Balance < 0.0) ? 0.0 : acc2.Balance);
        }

        public static double operator +(double amount, BaseAccount acc)
        {
            return amount + ((acc.Balance < 0.0) ? 0.0 : acc.Balance);
        }

        private string _Number;
        private double _Amount;
        private Person _Owner;

        public string Number
        {
            get { return _Number; }
            set { _Number = value; }
        }

        public double Balance
        {
            get { return _Amount; }
            private set { _Amount = value; }
        }

        public Person Owner
        {
            get { return _Owner; }
            set { _Owner = value; }
        }

        public BaseAccount(string Number, Person Owner) : this(Number, Owner, 0D)
        {

        }

        public BaseAccount(string Number, Person Owner, double Balance)
        {
            this.Balance = Balance;
            this.Number = Number;
            this.Owner = Owner;
        }

        public virtual void Withdraw(double Amount)
        {
            Withdraw(Amount, 0.0);
        }

        protected void Withdraw(double Amount, double creditLine)
        {
            if (this.Balance <= 0)
                // return; // to replace with an exception
                throw new InsufficientBalanceException();

            if (this.Balance - Amount < -creditLine)
                // return; // to replace with an exception
                throw new InvalidOperationException("You cannot withdraw that amount!");

            this.Balance -= Amount;
        }

        public void Deposit(double Amount)
        {
            if (Amount <= 0)
                // return; // to replace with an exception
                throw new ArgumentOutOfRangeException("The amount must be bigger than 0");

            this.Balance += Amount;
        }

        protected abstract double ComputeInterest();

        public void ApplyInterest()
        {
            this.Balance += ComputeInterest();
        }
    }
}
