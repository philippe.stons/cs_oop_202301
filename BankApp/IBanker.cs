﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    internal interface IBanker : ICustomer
    {
        string Number
        {
            get;
        }

        Person Owner
        {
            get;
        }

        void ApplyInterest();
    }
}
