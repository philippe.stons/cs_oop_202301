﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
    public class GenericTest<T> where T : BaseAccount
    {
        T type;

        public GenericTest(T type)
        {
            this.type = type;
        }

        public void MyMethod<U>() where U : struct
        {

        }
    }
}
