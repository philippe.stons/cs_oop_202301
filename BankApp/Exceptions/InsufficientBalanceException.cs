﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp.Exceptions
{
    public class InsufficientBalanceException: Exception
    {
        public InsufficientBalanceException(string msg = "Insufficient balance on the account!") : base(msg) 
        { }
    }
}
